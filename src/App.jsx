import React, { useState } from 'react';
// import './App.scss';
import { BrowserRouter as Router, Switch, Route, Link, useParams } from "react-router-dom";
import Home from './pages/Home/Home';
import History from './pages/History/History';
import InPictures from './pages/InPictures/InPictures';
import Navbar from './shared/components/Navbar/Navbar';
import Footer from './shared/components/Footer/Footer';
import { actualPageContext } from './shared/contexts/actualPageContext';

export default function App() {
  const [page, setPage] = useState([]);

  return (
    <Router>
      <actualPageContext.Provider value={[page, setPage]}>
        <Navbar />

        <Switch>
          <Route path="/mihistoria">
            <History />
          </Route>

          <Route path="/enfotos">
            <InPictures />
          </Route>

          <Route path="/">
            <Home />
          </Route>

        </Switch>
      </actualPageContext.Provider>
      
      <Footer />

    </Router>
  );
}

