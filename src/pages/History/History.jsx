import React, { useState, useEffect, useContext } from 'react';
import { actualPageContext } from '../../shared/contexts/actualPageContext';
import Navbar from '../../shared/components/Navbar/Navbar';

export default function History() {
    const [page, setPage] = useContext(actualPageContext);

    useEffect(() => {
        setPage('history');
    }, [])

    return (
        // <Navbar/>
        <div>
            Mi historia
        </div>
    )
}
