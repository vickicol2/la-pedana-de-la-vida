import React, { useState, useEffect, useContext } from 'react';
import { actualPageContext } from '../../shared/contexts/actualPageContext';
import { Link } from "react-router-dom";
import MyCarousel from '../../shared/components/MyCarousel/MyCarousel';
import Footer from "../../shared/components/Footer/Footer";
import './Home.scss';



export default function Home() {
    const [page, setPage] = useContext(actualPageContext);

    useEffect(() => {
        setPage('home');
    }, [])



    return (
        <div className="c-home">
            <div className="c-home__header">
                <div className="c-home__background"></div>

                <div className="c-home__img-header">
                    <img src="/img/veronica.png" alt="Maria Veronica Colmenares" />
                </div>

                <h1 className="c-home__first-title">La Pedana de la Vida</h1>
                <h2 className="c-home__second-title">Respiro... ¡Estoy Viva!</h2>

                <div className="c-home__bouquet">
                    <img src="/img/bouquet.svg" alt="Maria Veronica Colmenares" />
                </div>
            </div>

            {/* Esta es mi Historia */}
            <h1 className="c-home__third-title">Esta es mi Historia</h1>
            <div className="c-home__history">
                
                <div className="c-home__history-content d-flex flex-row">
                    <div className="c-home__img-herb1">
                        <img src="/img/herb4.svg" alt="" />
                    </div>
                    <p className="c-home__p">
                        ¡Hola, Soy Maria Verónica Colmenares Castro! Y te contaré como el 8 de octubre del 2014, con 21 años y siendo integrante de la Selección Nacional de Esgrima de Venezuela, Dios me tenía preparado el combate más díficil de mi vida.
                        Me preparaba para asistir con mi hermana a la universidad, donde comencé a estudiar la carrera de Ingeniería Electrónica en Computación.
                    </p>
                </div>

                <div className="c-home__history-content d-flex flex-row">
                    <div className="c-home__img-herb2">
                        <img src="/img/herb3.svg" alt="" />
                    </div>
                    <p className="c-home__p">
                        Ese día que les cuento, luego de tomar el desayuno, comencé a sentirme mareada y a eso le siguen un fuerte dolor de cabeza y pesadez en los miembros superiores, especialmente en el brazo derecho.
                        Mucho tiempo después sabría que lo que estaba ocurriendo no puede decirse de otro modo, era una "Hemorragia Cerebral en la fosa posterior por malformación congénita arteriovenosa".
                        Este adversario nació y vivió conmigo. Muy astuto, silencioso, traicionero, incisivo y mordaz, nunca se manifestó, pero cuando lo hizo casi cumple su cometido.
                    </p>

                </div>

                <div className="c-home__history-content d-flex flex-row">
                    <div className="c-home__img-herb3">
                        <img src="/img/herb2.svg" alt="" />
                    </div>
                    <p className="c-home__p">
                        Los exámenes arrojaban un resultado muy desalentador, y entré en criterios clínicos para muerte cerebral.
                        Fue entonces cuando los médicos invitaron a mi hermana gemela para que pasara a la sala de cuidados intensivos y tratara de captar alguna señal o respuesta que ellos no pudieron percibir.
                        <span>No la va a ver como el día de ayer-</span> le advirtió el Doctor. Tenga mucho valor, trate de no llorar, llámela por su nombre y apriete sus manos...
                    </p>
                </div>

                <Link className="c-home__bton-read" to="/mihistoria">Leer más...</Link>
            </div>

            {/* Mis Reportajes */}
            {/* <div className="c-home__hoja">
                <img src="/img/hoja.svg" alt="" />
            </div> */}
            <h1 className="c-home__third-title">Mis Reportajes</h1>
            <MyCarousel />

        </div>


    )
}