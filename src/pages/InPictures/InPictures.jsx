import React, { useState, useEffect, useContext } from 'react';
import { actualPageContext } from '../../shared/contexts/actualPageContext';

export default function InPictures() {
    const [page, setPage] = useContext(actualPageContext);

    useEffect(() => {
        setPage('pictures');
    }, [])


    return (
        <div>
            En fotos
        </div>
    )
}
