import React from 'react';
import './Footer.scss';

export default function Footer() {
    return (
        <div className="c-footer d-flex flex-row">
            <div className="c-footer__img col-3 col-sm-4">
            </div>

            <div className="c-footer__content d-flex flex-column">
                <div className="c-footer__logo">
                </div>

                <a className="c-footer__data mt-3" href="https://www.instagram.com/lapedanadelavida/"  target="_blank" rel="noreferrer"><span className="c-footer__span pedana-instagram"></span>@lapedanadelavida</a>
                <a className="c-footer__data mb-3" href="mailto:verocol21@gmail.com"><span className="c-footer__span pedana-gmail"></span> contacto@lapedanadelavida.com</a>
            </div>
        </div>
    )
}
