import React from 'react';
import { Carousel } from 'primereact/carousel';
import './MyCarousel.scss';

const contenidoSlide = [
    {
        src: "/img/marca.png",
        alt: "Maria Veronica: Una vida en el filo",
        link: "https://www.marca.com/primera-plana/2020/08/22/5f3a5a6d46163f50488b456c.html"
    },
    {
        src: "/img/antena3.png",
        alt: "Antena 3 Deportes",
        link: "https://www.instagram.com/p/CEe0L6jCd-V/"
    },
    {
        src: "/img/triangulodeportivo.png",
        alt: "El combate de Maria Veronica en su pedana de la vida",
        link: "https://triangulodeportivo.com/2020/08/29/combate-maria-veronica-colmenares-pedana-vida/"
    },
    {
        src: "/img/lavidadenos.png",
        alt: "Sobre esas segundas oportunidades",
        link: "https://www.lavidadenos.com/sobre-esas-segundas-oportunidades/"
    },
    {
        src: "/img/gabydenoche.png",
        alt: "Gaby de noche",
        link: "https://www.youtube.com/watch?v=SmtXjpD8GGo&t=20s"
    },
]

const responsiveOptions = [
    {
        breakpoint: '1024px',
        numVisible: 3,
        numScroll: 3
    },
    {
        breakpoint: '768px',
        numVisible: 2,
        numScroll: 2
    },
    {
        breakpoint: '560px',
        numVisible: 2,
        numScroll: 2
    }
];

export default function MyCarousel() {
    const templateSlide = (contenidoSlide) => {
        return <div className="c-slide">
            <a href={contenidoSlide.link} target="_blank" rel="noreferrer">
                <div className="c-slide__img">
                    <img src={contenidoSlide.src} alt={contenidoSlide.alt} />
                </div>
            </a>
            
        </div>
    }

    return (
        <div className="b-primereact-carousel">
            <Carousel value={contenidoSlide} itemTemplate={templateSlide} numVisible={3} numScroll={1} circular={false} responsiveOptions={responsiveOptions}></Carousel>
        </div>
    )
}
