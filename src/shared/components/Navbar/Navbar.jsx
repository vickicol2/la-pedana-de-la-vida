import React, { useEffect, useContext, useState } from 'react';
import './Navbar.scss';
import { Link } from "react-router-dom";
import { actualPageContext } from '../../contexts/actualPageContext';

export default function Navbar() {
  const [page, setPage] = useContext(actualPageContext);
  const [showMenu, setShowMenu] = useState(false);

  const toggleMenu = (e) => {
    if (e) {
      document.body.style.overflow = 'hidden';
      setShowMenu(true);
    } else {
      document.body.style.overflow = 'auto';
      setShowMenu(false);
    }
  }

  return (
    <nav className="c-navbar menu">
      <div className="c-navbar__logo">
        <img src="/img/logo-responsive.svg" alt="La pedana de la vida" />
      </div>

      <ul className={showMenu ? "c-navbar__ul c-navbar__ul--show" : "c-navbar__ul"}>
        <li className="c-navbar__li"><Link to="/" className={page === 'home' ? "c-navbar__link c-navbar__link--active" : 'c-navbar__link '} onClick={() => setShowMenu(false)}>Inicio</Link></li>
        <li className="c-navbar__li"><Link to="/mihistoria" className={page === 'history' ? "c-navbar__link c-navbar__link--active" : 'c-navbar__link '} onClick={() => setShowMenu(false)}>Mi Historia</Link></li>
        <li className="c-navbar__li"><Link to="/blog" className={page === 'blog' ? "c-navbar__link c-navbar__link--active" : 'c-navbar__link '} onClick={() => setShowMenu(false)}>Blog</Link></li>
        <li className="c-navbar__li"><Link to="/enfotos" className={page === 'pictures' ? "c-navbar__link c-navbar__link--active" : 'c-navbar__link '} onClick={() => setShowMenu(false)}>En redes</Link></li>

        <div className="c-navbar__img">
          <img src="/img/dot-leaf.svg" alt="" />
        </div>
      </ul>



      <input type="checkbox" name="toggle-bton" id="toggle-bton" className="c-navbar__checkbox" onChange={($e) => toggleMenu($e.target.checked)} />
      <label htmlFor="toggle-bton" className="c-navbar__toggle-bton" ><span className={showMenu ? "pedana-close" : "pedana-menu"}></span></label>
      {/* <button className="c-navbar__toggle-bton" onClick={() => toggleMenu()}><span className={showMenu ? "pedana-close" : "pedana-menu"}></span></button> */}
    </nav>
  )
}
